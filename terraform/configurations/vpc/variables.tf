variable "enable_s3_endpoint" {
    type = bool
}

variable "enable_dynamodb_endpoint" {
    type = bool
}