module "test_s3" {
  source = "git::http://git@gitlab.com/scdpublic/tft/module"
  bucket = var.bn
}

resource "aws_s3_bucket_policy" "deny" {
  bucket = module.test_s3.bucket_id

  policy = jsonencode({
    Version = "2012-10-17"
    Id      = "MYBUCKETPOLICY"
    Statement = [
      {
        Sid       = "IPAllow"
        Effect    = "Deny"
        Principal = "*"
        Action    = "s3:*"
        Resource = [
          module.test_s3.bucket_arn,
          "${module.test_s3.bucket_arn}/*",
        ]
        Condition = {
          IpAddress = {
            "aws:SourceIp" = "8.8.8.8/32"
          }
        }
      },
    ]
  })
}

module "newbucket" {
  source = "git::http://git@gitlab.com/scdpublic/tft/module"
  bucket = var.new_bucket
}
