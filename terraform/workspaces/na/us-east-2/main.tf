provider "aws" {
  region = var.region
}

terraform {
  backend "s3" {
    bucket = "scd-tf-state"
    key    = "na/us-east-2"
    region = "us-east-1"
  }
}

module "us-east-2" {
  source = "../../../configurations/bucket"

  bn         = "${var.region}-scd098-common"
  new_bucket = "${var.region}-scd-newbucket"
}
