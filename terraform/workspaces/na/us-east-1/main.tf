provider "aws" {
  region = var.region
}

terraform {
  backend "s3" {
    bucket = "scd-tf-state"
    key    = "na/us-east-1"
    region = "us-east-1"
  }
}

module "us-east-1" {
  source = "../../../configurations/bucket"

  bn         = "${var.region}-scd098-common"
  new_bucket = "${var.region}-scd-newbucket"
}

module "us-east-1_vpc" {
  source = "../../../configurations/vpc"

  enable_s3_endpoint       = true
  enable_dynamodb_endpoint = true
}
