# Terraform 
This is a POC for running terraform13 in a Gitlab pipeline that should work as a starting point.

Consumes modules:
s3, simple module from tft/module - accepts a single var
vpc, terraform public module repo

**file strucutre**
```
├── README.md
└── terraform
    ├── configurations
    │   ├── bucket
    │   │   ├── bucket.tf
    │   │   ├── main.tf
    │   │   └── variables.tf
    │   └── vpc
    │       ├── main.tf
    │       ├── variables.tf
    │       └── vpc.tf
    └── workspaces
        └── na
            ├── us-east-1
            │   ├── main.tf
            │   ├── terraform.tfvars
            │   └── variables.tf
            └── us-east-2
                ├── main.tf
                ├── terraform.tfvars
                └── variables.tf
```

This builds the following:

**region us-east-1**

main.tf in the workspace folder calls modules bucket and vpc.

**bucket:** us-east-1-scd098-common

**bucket:** us-east-1-scd-newbucket

**vpc:** tf-scd

------------------

**region us-east-2**

main.tf in the workspace folder calls only bucket

**bucket:** us-east-2-scd098-common 

**bucket:** us-east-2-scd-newbucket

### See the other repos for more info

  scdpublic/tft/module #s3 module

  scdpublic/tft/anchors #pipeline anchors

  https://github.com/terraform-aws-modules/terraform-aws-vpc/ #public module

```mermaid
classDiagram
    s3_module <-- Configurations : consumes
    s3_module : s3
    s3_module : s3_bucket_policy
    s3_module : +buckets()
    s3_module : +policy()

    VPC_module <-- Configurations : consumes
    VPC_module : vpc
    VPC_module : +s3_endpoint()
    VPC_module : +dynamo_endpoint()

    Configurations <-- Workspace_use1 : runs
    Configurations <-- Workspace_use2 : runs
    Configurations <-- Workspace_etc_etc_etc : runs
    Configurations <-- Workspace_use2_dev : runs
    Configurations : Config Modules
    Configurations : +s3(test_s3)
    Configurations : +s3(newbucket)
    Configurations : +vpc(vpc)

    class Workspace_use1{
      s3
      vpc
      +s3(us-east-1-scd098-common)
      +s3(us-east-1-scd-newbucket)
      +vpc(tf-use1)
    }

    class Workspace_use2{
      s3
      +s3(us-east-2-scd098-common)
      +s3(us-east-2-scd-newbucket)
    }
```